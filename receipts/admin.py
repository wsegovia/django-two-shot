from django.contrib import admin
from .models import Account, ExpenseCategory, Receipt

# Register your models here.


@admin.register(Account)
class AccountsAdmin(admin.ModelAdmin):
    pass


@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    pass


@admin.register(Receipt)
class ReceiptsAdmin(admin.ModelAdmin):
    pass
